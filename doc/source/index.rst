.. Good practices for writing python code documentation master file, created by
   sphinx-quickstart on Thu Feb  2 19:50:41 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pythonic ways of coding
=======================

.. toctree::
   :maxdepth: 3
   :numbered:
   :includehidden:
   :caption: Contents:

   oops/index
   advancePython/decorator
   advancePython/descriptor
   advancePython/generator
   .. advancePython/metaclass
   oops/super
   .. advancePython/index
   idiom/index



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

