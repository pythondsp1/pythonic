Inheritance with Super
======================

In this section, inheritance is discussed using super command. In most languages, the super method calls the parent class, whereas in python it is slightly different i.e. it consider the child before parent, as shown in this section. 


.. _superChildeBeforeParent:

Super : child before parent
---------------------------

Lets understand super with the help of an example. First, create a class Pizza, which inherits the DoughFactory for getting the dough as below, 


.. code-block:: python

    # pizza.py

    class DoughFactory(object):

        def get_dough(self):
            return 'white floor dough'


    class Pizza(DoughFactory):

        def order_pizza(self, *toppings):
            print("getting dough")

            # dough = DoughFactory.get_dough()
            ## above line is commented to work with DRY principle
            ## use super as below, 
            dough = super().get_dough()
            print("Making pie using '%s'" % dough)

            for topping in toppings:
                print("Adding %s" % topping)

    if __name__ == '__main__':
        Pizza().order_pizza("Pepperoni", "Bell Pepper")

Run the above code and we will get below output, 

.. code-block:: shell

    $ python -i pizza.py 

    getting dough
    Making pie using 'white floor dough'
    Adding Pepperoni
    Adding Bell Pepper
    >>> help(Pizza)
        Help on class Pizza in module __main__:

        class Pizza(DoughFactory)
         |  Method resolution order:
         |      Pizza
         |      DoughFactory
         |      builtins.object


.. note::

    The resolution order shows that the way in which python interpretor tries to find the methods i.e. it tries to find get_dough in Pizza class first; if not found there, it will go to DoughFactory. 


Now, create the another class in separate python file as below, 

.. code-block:: python

    # wheatDough.py

    from pizza import Pizza, DoughFactory


    class WheatDoughFactory(DoughFactory):

        def get_dough(self):
            return("wheat floor dough")

    class WheatPizza(Pizza, WheatDoughFactory):
        pass

    if __name__ == '__main__':
        WheatPizza().order_pizza('Sausage', 'Mushroom')


.. note:: 

    In python, Inheritance chain is not determine by the Parent class, but by the child class. 

    If we run the wheatDough.py, it will call the super command in class Pizza in pizza.py will not call his parent class i.e. DoughFactory, but the parent class of the child class i.e. WheatDoughFactory. This is called Dependency injection. 


.. important:: 

    * super consider the children before parents i.e. it looks methods in child class first, then it goes for parent class.
    * Next, it calls the parents in the order of inheritance. 
    * use keyword arguments for cooperative inheritance. 
      
    For above reasons, super is super command, as it allows to change the order of Inheritance just by modifying the child class, as shown in above example. 


.. rubric:: More Examples

For better understanding of the super command, some more short examples are added here, 


Inherit __init__
----------------

In the following code, class RectArea is inheriting the __init__ function of class RectLen. In the other word, length is set by class RectLen and width is set by class RectArea and finally area is calculated by class RectArea. 

.. code-block:: python

    # rectangle.py

    class RectLen(object):
        def __init__(self, length):
            self.length = length


    class RectArea(RectLen):
        def __init__(self, length, width):
            self.width = width
            super().__init__(length)
            print("Area : ", self.length * self.width)


    RectArea(4, 3)  # Area :  12

In the same way, the other functions of parent class can be called. In following code, printClass method of parent class is used by child class. 

.. code-block:: python

    # printClass.py

    class A(object):
        def printClassName(self):
            print(self.__class__.__name__)

    class B(A):
        def printName(self):
            super().printClassName()

    a = A()
    a.printClassName()  # A

    b = B()
    b.printClassName()  # B



.. note::
    
    In above code, print(self.__class__.__name__) is used for printing the class name, instead of print("A"). Hence, when child class will inherit this function, then __class__ will use the name of the child class to print the name of the class, therefore Line 15 prints "B" instead of A. 

Inherit __init__ of multiple classes
------------------------------------

In this section, various problems are discussed along with the solutions, which usually occurs during multiple inheritance.


Problem : super() calls __init__ of one class only
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In following example, class C is inheriting the class A and B. But, the super function in class C will inherit only one class init function i.e. init function of the class which occurs first in the inheritance order. 

.. code-block:: python

    #multipleInheritance.py

    class A(object):
        def __init__(self):
            print("A")

    class B(object):
        def __init__(self):
            print("B")

    class C(A, B):
        def __init__(self):
            super().__init__()

    # init of class B is not inherited
    c = C() # A


Solution 1
^^^^^^^^^^

Following is the first solution, where __init__ function of classes are invoked explicitly.  

.. code-block:: python

    #multipleInheritance.py

    class A(object):
        def __init__(self):
            print("A")

    class B(object):
        def __init__(self):
            print("B")

    class C(A, B):
        def __init__(self):
            A.__init__(self)  # self is required
            B.__init__(self)

    c = C() 
    # A 
    # B


Correct solution
^^^^^^^^^^^^^^^^

Following is the another solution of the problem; where super() function is added in both the classes. Note that, the super() is added in class B as well, so that class(B, A) will also work fine. 

.. code-block:: python 

    #multipleInheritance.py

    class A(object):
        def __init__(self):
            print("reached A")
            super().__init__()
            print("A")

    class B(object):
        def __init__(self):
            print("reached B")
            super().__init__()
            print("B")

    class C(A, B):
        def __init__(self):
            super().__init__()

    c = C() 
    # reached A
    # reached B
    # B
    # A


The solution works fine here because in Python super consider the child before parent, which is discussed in Section :ref:`superChildeBeforeParent`. Please see the order of output as well. 


.. _mathProblem:

Math Problem
------------

This section summarizes the above section using math problem. Here, we want to calculate (x * 2 + 5), where x = 3. 

Solution 1
^^^^^^^^^^

This is the first solution, __init__ function of two classes are invoked explicitly. The only problem here is that the solution does not depend on the order of inheritance, but on the order of invocation, i.e. if we exchange the lines 15 and 16, the solution will change.

.. code-block:: python

    # mathProblem.py

    class Plus5(object):
        def __init__(self, value):
            self.value = value + 5

    class Multiply2(object):
        def __init__(self, value):
            self.value = value * 2

    class Solution(Multiply2, Plus5):
        def __init__(self, value):
            self.value = value
            
            Multiply2.__init__(self, self.value)
            Plus5.__init__(self, self.value)


    s = Solution(3)
    print(s.value)  # 11 
  

problem with super
^^^^^^^^^^^^^^^^^^

One of the problem with super is that, the top level super() function does not work if it has some input arguments. If we look the output of following code carefully, then we will find that error is generated after reaching to class Plus5. When class Plus5 uses the super(), then it calls the metaclass's (i.e. object) __init__ function, which does not take any argument.  Hence it generates the error 'object.__init__() takes no parameters'. 

To solve this problem, we need to create another class as shown in next section. 


.. code-block:: python

    # mathProblem.py

    class Plus5(object):
        def __init__(self, value):

            print("Plus 5 reached")
            self.value = value + 5

            super().__init__(self.value)
            print("Bye from Plus 5")

    class Multiply2(object):
        def __init__(self, value):
            
            print("Multiply2 reached")
            self.value = value * 2
            
            super().__init__(self.value)
            print("Bye from Multiply2")

    class Solution(Multiply2, Plus5):
        def __init__(self, value):
            self.value = value
            super().__init__(self.value)


    s = Solution(3)
    print(s.value)  

    # Multiply2 reached
    # Plus 5 reached
    # [...]
    # TypeError: object.__init__() takes no parameters

Solution 2
^^^^^^^^^^

To solve the above, we need to create another class, and inherit it in classes Plus5 and Multiply2 as below, 

In below code, MathClass is created, whose init function takes one argument. Since, MathClass does not use super function, therefore above error will not generate here. 

Next, we need to inherit this class in Plus5 and Multiply2 for proper working of the code, as shown below. Further, below code depends on order of inheritance now.

.. code-block:: python

    # mathProblem.py

    class MathClass(object):
        def __init__(self, value):
            print("MathClass reached")
            self.value = value
            print("Bye from MathClass")

    class Plus5(MathClass):
        def __init__(self, value):
            print("Plus 5 reached")
            self.value = value + 5
            super().__init__(self.value)
            print("Bye from Plus 5")

    class Multiply2(MathClass):
        def __init__(self, value):
            print("Multiply2 reached")
            self.value = value * 2
            super().__init__(self.value)
            print("Bye from Multiply2")

    class Solution(Multiply2, Plus5):
        def __init__(self, value):
            self.value = value
            super().__init__(self.value)


    s = Solution(3)
    print(s.value)  # 11 

    # Multiply2 reached
    # Plus 5 reached
    # MathClass reached
    # Bye from MathClass
    # Bye from Plus 5
    # Bye from Multiply2
    # 11

    ## uncomment below to see the Method resolution order
    print(help(Solution))
    # class Solution(Multiply2, Plus5)
    #  |  Method resolution order:
    #  |      Solution
    #  |      Multiply2
    #  |      Plus5
    #  |      MathClass
    #  |      builtins.object



Conclusion
----------

In this tutorial, we saw the functionality of the super() function. It is shown that super() consider the child class first and then parent classes in the order of inheritance. Also, help command is used for observing the 'method resolution order' i.e. hierarchy of the inheritance. 

