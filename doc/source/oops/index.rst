Agile methodology
=================

Do not try to add lots of features to a class at once, i.e. use agile method. Ship the product and wait for feedbacks and requirements. Add features only when asked for, as python provided various methods to solve the problem as shown in this tutorial. 

Here, we will understand the actual logic behind various pieces of python e.g. instances, variables, method and @property etc. Also, we will see the combine usage of these pieces to complete the complex designs.


Instance variable
-----------------


Note that, init is not the constructor; it is initializer, which initialize the the instance variables.
Further, 'self' is the instance and 'self.radius' is the instance variable which must be unique, in the following code,
 
.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        """ Here we will understand the actual logic behind various pieces of python
        e.g. instances, variables, method and @property etc. Also, we will see
        the combine usage of these pieces to complete the complex designs. """

        def __init__(self, radius):
            """ init is not the constructor, it is initializer, which
            initialize the the instance variables.

            self : is the instance and
            self.radius : is the instance variable which must be unique.

            init takes the existing instance 'self' and populates it with
            radius and stored in a dictionary.
            """

            self.radius = radius  # instance variable

        def area(self):
            return(math.pi * self.radius**2)


Class variable
--------------

class variables are used for shared data. It is good to put shared data on class level rather than instance level.

.. code-block:: python

    # circle.py

    import math

    class Circle(object):

        """ class variables are used for shared data. It is good to put
        shared data on class level rather than instance level. """
        center = 0.0  # class variable

        [...]


Object of class
---------------

Creating is object of class. Note that, every object create a dictionary which stores the instance variable in it, as shown below,

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]


    def main():

        # Center of the circle :  (0, 0)
        print("Center of the circle : ", Circle.center)
        c = Circle(10)
        print("Radius : ", c.radius)  # Radius :  10
        print("Area : ", c.area())  # Area :  314.1592653589793

        print(c.__dict__)  # {'radius': 10}


    if __name__ == '__main__':
        main()


Calculate average area
----------------------

Now, first client uses our class for calculating the average area of the circle. He has no problem until at the end of tutorial. We will solve his problem, when he will raise it. 

.. code-block:: python

    # avgArea.py

    from random import random, seed
    from circle import Circle

    # seed for random number generation
    seed(3)

    # generate 10 circles
    n = 10

    circles = [Circle(random()) for i in range(n)]
    avg = sum([c.area() for c in circles])/n

    print("Average Area : %.2f " % avg)  # Average Area : 0.65


Add perimeter method in class
-----------------------------

Lets add one more method to class i.e. perimeter. 

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        def perimeter(self):
            return (2 * math.pi * self.radius)


    def main():

        # Center of the circle :  (0, 0)
        print("Center of the circle : ", Circle.center)
        c = Circle(10)
        print("Radius : ", c.radius)  # Radius :  10
        print("Area : ", c.area())  # Area :  314.1592653589793


    if __name__ == '__main__':
        main()


Calculate area of rubber sheet in cold and warm weather
-------------------------------------------------------

Now other client uses perimeter method to calculate the perimeter in hot and cold weather. 

Note that, we expose the attribute to users i.e. radius. And user modified the attribute using 'c.radius *= 1.1'. Hence, be aware of such changes while creating the codes. Also, if we modify the c.radius, e.g. limit it to 4 decimal points, then it may break the clients code. 


.. code-block:: python

    # rubberSheet.py

    from circle import Circle

    cuts = [0.1, 0.3, 0.5]
    circles = [Circle(r) for r in cuts]

    for c in circles:
        print("Radius : ", c.radius)
        print("perimeter : ", c.perimeter())
        print("area in cold : ", c.area())
        c.radius *= 1.1  # increase in radius in warm
        print("area in warm : ", c.area())

        # Radius :  0.1
        # perimeter :  0.6283185307179586
        # area in cold :  0.031415926535897934
        # area in warm :  0.038013271108436504
        # Radius :  0.3
        # perimeter :  1.8849555921538759
        # area in cold :  0.2827433388230814
        # area in warm :  0.34211943997592853
        # Radius :  0.5
        # perimeter :  3.141592653589793
        # area in cold :  0.7853981633974483
        # area in warm :  0.9503317777109126


.. note::

    In Python, we do not hide anything from the users, as oppose to other OOPs language e.g. Java and C++. In those languages data hiding is the key concept and users do not have direct access to attributes; whereas in Python users have direct access to attributes.

    If clients started to modify the attributes, then modifying the actual codes is serious problem in Java and C++, therefore private and protected variables are required there. But, python provides various features to handle such situations without breaking the client code. 


Inheritence
-----------

Third client inherit our class and override the perimeter method. While writing classes, be aware that clients will make subclasses from it and override the existing methods. 


.. code-block:: python

    # modifiedPerimeter

    from circle import Circle

    class Tire(Circle):
        'Tires are circle with modified perimeter'

        def perimeter(self):
            'perimtere is correct by a factor of 1.25'
            return Circle.perimeter(self) * 1.25


    t = Tire(10)
    print("Radius : ", t.radius)  # Radius :  10
    print("Area : ", t.area())  # Area :  314.1592653589793
    print("Perimeter : ", t.perimeter())  # Perimeter :  78.53981633974483


Multiple constructor
--------------------

Fourth client want to use diameter as input instead of radius. Now, we will start adding features as requirement from clients. 

Class methods are used for creating multiple constructor. But, before seeing the implement, let's consider the the following case, which needs the multiple constructor as a solution, 

.. note:: 
    
    If we have the client, who wants to calculate the area based on the diagonal (not based on radius). Then we need to modify the constructor, which takes diagaonal as input argument. If we modify the 'init' function in Circle class, then all the other clients code will break. In such situation, mutliple constructor are quite useful. 

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        @classmethod  # multiple constructor
        def from_diameter(cls, diameter):
            radius = diameter / 2
            return(cls(radius))

        [...]

Now, the client can use the all the methods of class using diameter as well as below,

.. code-block:: python

    # diameterArea.py

    from circle import Circle

    c =  Circle.from_diameter(20)
    print(c.radius)  # 10
    print(c.area())  # 314.1592653589793


Static methods
--------------

Clients want some features which has no relation with class methods, but it's handy for him to have some features in the package. In such cases, use static methods. 

Sometimes, we need some features in the class, which has nothing do to with the class. We need to add those methods, just because clients want it, for easy operations. For example, clients want to convert the radius from meter to cm, which has no direct relation with rest of the methods as mathematical formula is independent of the units. In such cases, static methods should be used. 

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        @staticmethod
        def meter_cm(meter):
            return(100*meter)
        [...]

.. note::

    There is not point to create meter_cm method as 'meter_cm(self, meter)' and store it in the dictionary, as we are not going to use it anywhere in the class. We added this to meet the client's requirement only, hence it should be added as simple definition (not as methods), which can be done 
    using @staticmethod. In the other words, static methods are used to attached functions to classes. 


We can use static function as below, 

.. code-block::  python

    # diameterArea.py

    from circle import Circle

    c =  Circle.from_diameter(20)
    print(c.radius)  # 10
    print(c.area())  # 314.1592653589793

    print(c.meter_cm(10))  # 1000


Modify area method
------------------

Next, if client request that he wants to calculate the area based on the perimeters i.e. first calculate the perimeter and then calculate the radius again based on perimeter and finally calculate the area based on this radius. This is called micro-managing, where the client tells you to the way in which logic should be implemented. 

This can be implemented easily as shown below,

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        def area(self):
            # return(math.pi * self.radius**2)
            p = self.perimeter()
            r = p / (math.pi * 2)
            return (math.pi * r ** 2)

        def perimeter(self):
            [...]

.. error:: 


    Above solution looks pretty simple, but it will break the code in 'modifiedPerimeter.py' file, as it is modifying the perimeter by a factor of 1.25 by overriding the perimeter method. Therefore, for calculating the area in modifiedPerimeter.py file, python interpretor will use the child class method `perimeter' for getting the value of perimeter. 

    * In modifiedPerimeter.py, the area will be wrongly calculated, because they are modifying the perimeter for their usage. Now, radius will be calculated by new perimeter (as they override the perimeter method) and finally area will be modified due to change in radius values.
 

Following are the comparisons of the outputs of modifiedPerimeter.py file, 

.. code-block:: shell
    
    Parameters   Actual values             New Values
    Radius :           10                    10
    Area :       314.1592653589793       490.8738521234052      (wrong)
    Perimeter :  78.53981633974483       78.53981633974483

The above problem can be solved by making a copy of perimeter method i.e. '_perimeter' and use the copied file for calculation, instead of original, as shown below. In this way, the others code will not break as _perimeter method is not available in the child class. 

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        def area(self):
            # return(math.pi * self.radius**2)
            p = self._perimeter()  # use copied method here
            r = p / (math.pi * 2)
            return (math.pi * r ** 2)
        
        def perimeter(self):
            return (2 * math.pi * self.radius)
        
        _perimeter = perimeter  #  make a copy of perimeter method
        
        [...]

If we execute the modifiedPerimeter.py again, the following outputs will be received, 

.. code-block:: shell
    
    Parameters   Actual values             New Values
    Radius :           10                    10
    Area :       314.1592653589793       314.1592653589793      (correct)
    Perimeter :  78.53981633974483       78.53981633974483


Private attributes are not for privatizing the attributes
---------------------------------------------------------

If a copy of perimeter is made in modifiedPerimeter.py as well, i.e. they are also using a copy of perimeter for their purposes, then the situation will become same as previous, shown below, 

.. code-block:: python

    # modifiedPerimeter

    from circle import Circle

    class Tire(Circle):
        'Tires are circle with modified perimeter'

        def perimeter(self):
            'perimtere is correct by a factor of 1.25'
            return Circle.perimeter(self) * 1.25

        _perimeter = perimeter  # copy of perimeter in child class

        [...]

Now, execute the code, and we will get the wrong output again, 


.. code-block:: shell
    
    Parameters   Actual values             New Values
    Radius :           10                    10
    Area :       314.1592653589793       490.8738521234052      (wrong)
    Perimeter :  78.53981633974483       78.53981633974483

.. important:: 

    _ _ is not designed for making the attribute private, but for renaming the attribute with class name to avoid conflicts due to same name as we see in above example, where using same name i.e. _perimeter in both parent and child class resulted in the wrong answer. 

    If we use _ _perimeter instead of _perimeter, then _ _perimeter will be renamed as _ClassName__perimeter. Therefore, even parent and child class uses the same name with two underscore, the actual name will be differed because python interpretor will add the ClassName before those names, which will make it different from each other.  

Following are the modified code along with the outputs, 

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        def area(self):
            # return(math.pi * self.radius**2)
            p = self.__perimeter()  # use copied method here
            r = p / (math.pi * 2)
            return (math.pi * r ** 2)
        
        def perimeter(self):
            return (2 * math.pi * self.radius)
        
        __perimeter = perimeter  #  make a copy of perimeter method
        
        [...]

.. code-block:: python

    # modifiedPerimeter

    from circle import Circle

    class Tire(Circle):
        'Tires are circle with modified perimeter'

        def perimeter(self):
            'perimtere is correct by a factor of 1.25'
            return Circle.perimeter(self) * 1.25

        __perimeter = perimeter  # copy of perimeter in child class

        [...]

If we execute the modifiedPerimeter.py again, the following outputs will be received, 

.. code-block:: shell
    
    Parameters   Actual values             New Values
    Radius :           10                    10
    Area :       314.1592653589793       314.1592653589793      (correct)
    Perimeter :  78.53981633974483       78.53981633974483




.. note::
    
    _ _ is designed for freedom not for privacy as we saw in this section...


@property
---------

Suppose, we get a rule from client as follows, 

* we are not allowed to store radius as instance variable, 
* instead convert radius into diameter and save it as instance variable.  

This condition will raise following two problems, 

* Main problem here is that, other clients already started using this class and have access to attribute 'radius'. Now, if we replace the key 'radius' with 'diameter', then there code will break immediately.  
* Also, we need to update all our code as everything is calculated based on radius; and we are going to remove these attribute from diameter. 
  
This is the main reason for hiding attributes in java and c++, as these languages have no easy solution for this. Hence, these languages use getter and setter method. 




.. note::
    
    But in python this problem can be solved using @property. @property is used to convert the attribute access to method access.

    In the other words, as radius will be removed from instance variable list after meeting the need of the client, therefore it can not be used anymore. But, @property decorator will convert the attribute access to method access, i.e. the dot operator will check for methods with @property as well. In this way, the code of other client will not break, as shown next. 

.. code-block:: python

    # circle.py

    import math

    class Circle(object):
        [...]

        def __init__(self, radius):
            [...]

            self.radius = radius  # instance variable

        # self.radius (or c.radius) will invoke following function
        @property
        def radius(self):
            return self.diameter/2

        # save diameter as instance variable instead of radius
        @radius.setter  # 
        def radius(self, radius):
            self.diameter = radius * 2

        [...]

    def main():

        [...]
        c = Circle(10)
        print("Radius : ", c.radius)  # Radius :  10
        print("Area : ", c.area())  # Area :  314.1592653589793
        print("Diameter : ", c.diameter)  # 20

        # only diameter is saved in object dictionary
        print(c.__dict__)  # {'diameter': 20}

    if __name__ == '__main__':
        main()


__slots___
----------


Note that every object creates a dictionary as shown in previous code. If our first client wants to take the average fo 100000 circle in avgArea.py file, then he will raise the memory issue caused by 100000 dictionaries. Use __slots___ to avoid making dictionaries for each object and solve his problem. It will create only a pointer to object now. Note that print(c.__dict__) will generate error now as no dictionary is create now, hence comment that line, as shown below, 

.. code-block:: python 

    # circle.py

    import math

    class Circle(object):
        """ Here we will understand the actual logic behind various pieces of python
            e.g. instances, variables, method and @property etc. Also, we will see
            the combine usage of these pieces to complete the complex designs. """

        """ class variables are used for shared data. It is good to put
        shared data on class level rather than instance level. """
        center = (0, 0)  # class variable

        __slots__ = ['diameter']

        def __init__(self, radius):
            """ init is not the constructor, it is initializer, which
            initialize the the instance variables.

            self : is the instance and
            self.radius : is the instance variable which must be unique.

            init takes the existing instance 'self' and populates it with
            radius and stored in a dictionary.
            """

            self.radius = radius  # instance variable

        # self.radius (or c.radius) will invoke following function
        # instead of looking at the dictionary
        @property
        def radius(self):
            return self.diameter/2

        # save diameter in dictionary instead of radius
        @radius.setter
        def radius(self, radius):
            self.diameter = radius * 2

        def area(self):
            # return(math.pi * self.radius**2)
            p = self.__perimeter()
            r = p / (math.pi * 2)
            return (math.pi * r ** 2)
        

        def perimeter(self):
            return (2 * math.pi * self.radius)
        __perimeter = perimeter

        @classmethod  # multiple constructor
        def from_diameter(cls, diameter):
            radius = diameter / 2
            # don't use Class radius here, 
            # otherwise subclass can not use this constructor
            return(cls(radius))

        @staticmethod
        def meter_cm(meter):
            return(100*meter)

    def main():

        # Center of the circle :  (0, 0)
        print("Center of the circle : ", Circle.center)
        c = Circle(10)
        print("Radius : ", c.radius)  # Radius :  10
        print("Area : ", c.area())  # Area :  314.1592653589793
        print("Diameter : ", c.diameter)  # 20

        ## only diameter is saved in dictionary  
        # print(c.__dict__)  # {'diameter': 20}

    if __name__ == '__main__':
        main()


Methods, staticmethod and classmethods
--------------------------------------

We saw the usage of methods, static methods and class methods in previous sections. Let's compare the results these methods again, with one more example as below. 

Please see the comments and notice how different methods use different values of x for adding two numbers, 

.. code-block:: python

    # mehodsEx.py

    class Add(object):
        x = 9  # class variable

        def __init__(self, x):
            self.x = x  # instance variable

        def addMethod(self, y):
            print("method : ", self.x + y)

        @classmethod
        # as convention, cls must be used for classmethod, instead of self
        def addClass(self, y):
            print("classmethod : ", self.x + y)

        @staticmethod
        def addStatic(y):
            print("staticmethod : ", x + y)


    # method
    m  = Add(4)
    # for method, above x = 4 (i.e Add(4)), will be used for addition
    m.addMethod(10)  # method :  14

    # classmethod
    c = Add(4)
    # for class method, class variable x = 9, will be used for addition
    c.addClass(10)  # clasmethod : 19

    # for static method, below x=20, will be used for addition
    x = 20
    s = Add(4)
    s.addStatic(10)  # staticmethod : 30

.. note::
    
    We can observe the following diffrences in these three methods from above code, 

    * **method** : it uses the instance variable (self.x) for addition, which is set by __init__ function. 
    * **classmethod** : it uses class variable for addition. 
    * **staticmethod** : it uses the value of x which is defined in main program (i.e. outside the class ). If x = 20 is not defined, then NameError will be generated. 


Conclusion
----------

In this tutorial, we saw the example of agile method of designing; where we started with simple class 'Circle' with only one method 'area' and shipped it to customers. Then, based on feedback and requirements our class grows significantly. Lastly, we saw that Python provides a good solution for each problem. 