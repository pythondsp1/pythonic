Generators
==========

Any function that uses the 'yield' statement is the generator. Each yield temporarily suspends processing, remembering the location execution state (including local variables and pending try-statements). When the generator iterator resumes, it picks-up where it left-off (in contrast to functions which start fresh on every invocation).

Feed iterations
---------------

Typically, it is used to feed iterations as below, 

.. code-block:: python

    # generatorEx.py

    def countdown(n):
        while (n>0):
            yield n
            n -= 1

    for x in countdown(5):
        print(x)  # 5 4 3 2 1

    print()
    c = countdown(3)
    print(next(c))  # 3
    print(next(c))  # 2
    print(next(c))  # 1
    print(next(c))
    # Traceback (most recent call last):
    #   File "rectangle.py", line 16, in <module>
    #     print(next(c))
    # StopIteration

 If the generator exits without yielding another value, a StopIteration exception is raised.

Receive values
--------------

'yield' can receive value too. Calling the function creates the generator instance, which needs to be advance to next yield using 'next' command. Then generator is ready to get the inputs, as shown below, 

.. code-block:: python

    # generatorEx.py

    def rxMsg():
        while True:
            item = yield
            print("Message : ", item)

    msg = rxMsg()
    print(msg)  # <generator object rxMsg at 0xb7049f8c>

    next(msg)
    # send : Resumes the execution and “sends” a value into the generator function
    msg.send("Hello")
    msg.send("World")

Send and receive values
-----------------------

Both send and receive message can be combined together in generator. Also, generator can be closed, and next() operation will generate error if it is used after closing the generator,  as shown below,

.. code-block:: python

    # generatorEx.py

    def rxMsg():
        while True:
            item = yield
            yield("Message Ack: " + item)


    msg = rxMsg()

    next(msg)
    m1 = msg.send("Hello")
    print(m1)  # Message Ack: Hello

    next(msg)
    m2 = msg.send("World")
    print(m2)  # Message Ack: World

    msg.close()  # close the generator

    next(msg)
    # Traceback (most recent call last):
    #   File "rectangle.py", line 21, in <module>
    #     next(msg)
    # StopIteration


Return values in generator
--------------------------

Generator can return values which is displayed with exception, 

.. code-block:: python

    # generatorEx.py

    def rxMsg():
        while True:
            item = yield
            yield("Message Ack: " + item)
            return "Thanks"


    msg = rxMsg()

    next(msg)
    m1 = msg.send("Hello")
    print(m1)  # Message Ack: Hello

    next(msg)
    # Traceback (most recent call last):
    #   File "rectangle.py", line 16, in <module>
    #     next(msg)
    # StopIteration: Thanks

    m2 = msg.send("World")
    print(m2)  


'yield from' command
--------------------

When yield from <expr> is used, it treats the supplied expression as a subiterator. All values produced by that subiterator are passed directly to the caller of the current generator’s methods, 

.. code-block:: python

    # generatorEx.py

    def chain(x, y):
        yield from x 
        yield from y

    a = [1, 2, 3]
    b = [20, 30]

    for i in chain(a, b):
        print(i, end=' ')  # 1, 2, 3, 20, 30

    print()
    for i in chain(chain(a, a), chain(b, a)):
        print(i, end=' ')  # 1 2 3 1 2 3 20 30 1 2 3 

