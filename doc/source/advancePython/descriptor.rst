Descriptors
===========

Descriptor gives us the fine control over the attribute access. It allows us to write reusable code that can be shared between the classes as shown in this tutorial. 

Problems with @property
-----------------------

Before beginning the descriptors, let's look at the @property decorator and it's usage along with the problem. Then we will see the descriptors in next section. 


.. code-block:: python

    # square.py

    class Square(object):
        def __init__(self, side):
            self.side = side
            
        def aget(self):
            return self.side ** 2
        
        def aset(self, value):
            print("can not set area")
        
        def adel(self):
            print("Can not delete area")
        
        area = property(aget, aset, adel, doc="Area of sqare")


    s = Square(10)
    print(s.area)  # 100

    s.area = 10  # can not set area

    del s.area  # can not delete area

Note that in above code, no bracket is used for calculating the area i.e. 's.area' is used, instead of s.area(); because area is defined as property, not as a method. 


Above code can be rewritten using @property decorator as below, 

.. code-block:: python

    # square.py

    class Square(object):
        """ A square using property with decorator"""
        def __init__(self, side):
            self.side = side
        
        @property
        def area(self):
            """Calculate the area of the square"""
            return self.side ** 2
        
        # name for setter and deleter (i.e. @area) must be same 
        # as the method for which @property is used i.e. area here
        @area.setter
        def area(self, value):
            """ Do not allow set area directly"""
            print("Can not set area")
        
        @area.deleter
        def area(self):
            """Do not allow deleting"""
            print("Can not delete area")


    s = Square(10)
    print(s.area)  # 100

    s.area = 10  # can not set area

    del s.area  # can not delete area


Note that, @area.setter and @area.deleter are optional here. We can stop writing the code after defining the @property. In that, case setter and deleter option will generate standard exception i.e. attribute error here. If we want to perform some operations, then setter and deleter options are required. 

Further, code is repetitive because @property, setter and deleter are the part of same method here i.e. area. 

We can merge all three methods inside one method as shown next. But before looking at that example, let's understand two python features first i.e.  **kwargs and locals(). 


.. rubric:: **kwargs

**kwargs converts the keyword arguments into dictionary as shown below, 

.. code-block:: python

    def func(**kwargs):
        print(kwargs)

    func(a=1, b=2)  # {'a': 1, 'b': 2}



.. rubric:: locals()

Locals return the dictionary of local variables, as shown below, 

.. code-block:: python

    def mathEx(a, b):
        add = a + b
        diff = a - b
        print(locals())

    mathEx(3, 2)  # {'a': 3, 'add': 5, 'b': 2, 'diff': 1}

Now, we can implement all the get, set and del method inside one method as shown below, 

.. code-block:: python

    # square.py

    def nested_property(func):
        """ Nest getter, setter and deleter"""
        names = func()

        names['doc'] = func.__doc__
        return property(**names)


    class Square(object):
        """ A square using property with decorator"""
        def __init__(self, side):
            self.side = side
        
        @nested_property
        def area():
            """Calculate the area of the square"""

            def fget(self):
                """ Calculate area """
                return self.side ** 2
        
            def fset(self, value):
                """ Do not allow set area directly"""
                print("Can not set area")
        
            def fdel(self):
                """Do not allow deleting"""
                print("Can not delete area")

            return locals()

    s = Square(10)
    print(s.area)  # 100

    s.area = 10  # can not set area

    del s.area  # can not delete area


.. note::

    @propery is good for performing certain operations before get, set and delete operation. But, we need to implement it for all the functions separately and code becomes repetitive for larger number of methods. In such cases, descriptors can be useful.


Data Descriptors
----------------


.. code-block:: python

    # data_descriptor.py

    class DataDescriptor(object):
        """ descriptor example """
        def __init__(self):
            self.value = 0

        def __get__(self, instance, cls):
            print("data descriptor __get__")
            return self.value   

        def __set__(self, instance, value):
            print("data descriptor __set__")
            try:
                self.value = value.upper()
            except AttributeError:
                self.value = value

        def __delete__(self, instance):
            print("Can not delete")

    class A(object):
        attr =  DataDescriptor()



    d = DataDescriptor()
    print(d.value)  # 0

    a = A()
    print(a.attr)
    # data descriptor __get__
    # 0

    # a.attr is equivalent to below code
    print(type(a).__dict__['attr'].__get__(a, type(a)))
    # data descriptor __get__
    # 0

    # set will upper case the string
    a.attr = 2  # 2 
    # lazy loading: above o/p will not display if
    # below line is uncommented
    a.attr = 'tiger' # TIGER
    print(a.__dict__) # {}

    # Following are the outputs of above three commands 
    # data descriptor __set__
    # data descriptor __set__
    # {}
    # data descriptor __get__
    # data descriptor __get__
    # TIGER

.. note::

    * Note that object 'd' does not print the 'data descriptor __get__' but object of other class i.e. A prints the message. In the other words, descriptor can not use there methods by its' own. **Other's class-attributes** can use descriptor's methods as shown in above example. 

Also, see the outputs of last three commands. We will notice that, 

.. note::

    * The set values are not store in the instance dictionary i.e. print(a.__dict__) results in empty dictionary. 
    * Further, a.attr = 2 and a.attr 'tiger' performs the set operation immediately (see the __set__ message at outputs), but __get__ operations are performed at the end of the code, i.e. first print(a.__dict__) outputs are shown and then get operations is performed. 
    * Lastly, set operation stores only last executed value, i.e. only TIGER is printed at the end, but not 2.

non-data descriptor
-------------------

non-data descriptor stores the assigned values in the dictionary as shown below, 

.. code-block:: python

    # non_data_descriptor.py

    class NonDataDescriptor(object):
        """ descriptor example """
        def __init__(self):
            self.value = 0

        def __get__(self, instance, cls):
            print("non-data descriptor __get__")
            return self.value + 10   

    class A(object):
        attr =  NonDataDescriptor()

    a = A()
    print(a.attr)
    # non-data descriptor __get__
    # 10

    a.attr = 3
    a.attr = 3
    print(a.__dict__) # {'attr': 4}


.. important:: 

    * In Non-data descriptor, the assigned values are stored in instance dictionary (and only last assigned value is stored in dictionary); whereas data descriptor assigned values are stored in descriptor dictionary because the set method of descriptor is invoked. 

__getattribute__ breaks the descriptor usage
--------------------------------------------


In below code, __getattribute__ method is overridden in class Overriden. Then, instance of class Overriden is created and finally the descriptor is called at Line 18. In this case, __getattribute__ method is invoked first and does not give access to descriptor. 

.. code-block:: python

    # non_data_descriptor.py

    class NonDataDescriptor(object):
        """ descriptor example """
        def __init__(self):
            self.value = 0

        def __get__(self, instance, cls):
            print("non-data descriptor __get__")
            return self.value + 10   

    class Overriden(object):
        attr =  NonDataDescriptor()
        def __getattribute__(self, name):
            print("Sorry, No way to reach to descriptor!")

    o = Overriden()
    o.attr  # Sorry, No way to reach to descriptor!

.. note::

    Descriptors can not be invoked if __getattribute__ method is used in the class as shown in above example. We need to find some other ways in such cases. 

Use more than one instance for testing
--------------------------------------

Following is the good example, which shows that test must be performed on more than one object of a classes. As following code, will work fine for one object, but error can be caught with two or more objects only. 

.. code-block:: python

    # Examples.py

    class DescriptorClassStorage(object):
        """ descriptor example """
        def __init__(self, default = None):
            self.value = default

        def __get__(self, instance, cls):
            return self.value      
        def __set__(self, instance, value):
            self.value = value

    class StoreClass(object):
        attr =  DescriptorClassStorage(10)


    store1 =  StoreClass()
    store2 =  StoreClass()

    print(store1.attr, store2.attr)  # 10, 10

    store1.attr = 30

    print(store1.attr, store2.attr) # 30, 30

In above code, only store1.attr is set to 30, but value of store2.attr is also changes. This is happening because, in data-descriptors values are stored in descriptors only (not in instance dictionary as mentioned in previous section). 

Examples
--------

Write a descriptor which allows only positive values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Following is the code to test the positive number using descriptor, 

.. code-block:: python

    # positiveValueDescriptor.py

    class PositiveValueOnly(object):
        """ Allows only positive values """

        def __init__(self):
            self.value = 0

        def __get__(self, instance, cls):
            return self.value

        def __set__(self, instance, value):
            if value < 0 : 
                raise ValueError ('Only positive values can be used')
            else:
                self.value = value

    class Number(object):
        """ sample class that uses PositiveValueOnly descriptor"""

        value = PositiveValueOnly()


    test1 = Number()
    print(test1.value)  #  0

    test1.value = 10
    print(test1.value)  #  0

    test1.value = -1
    # [...]
    # ValueError: Only positive values can be used



Passing arguments to decorator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

IN previous codes, no arguments were passed in the decorator. In this example, taxrate is passed in the decorator and total price is calculated based on tax rate. 

.. code-block:: python

    # taxrate.py

    class Total(object):
        """ Calculate total values """

        def __init__(self, taxrate = 1.20):
            self.rate = taxrate

        def __get__(self, instance, cls):
            # net_price * rate
            return instance.net * self.rate

        # override __set__, so that there will be no way to set the value
        def __set__(self, instance, value):
            raise NoImplementationError("Can not change value")

    class PriceNZ(object):
        total = Total(1.5)

        def __init__(self, net, comment=""):
            self.net = net
            self.comment = comment

    class PriceAustralia(object):
        total = Total(1.3)

        def __init__(self, net):
            self.net = net


    priceNZ = PriceNZ(100, "NZD")  
    print(priceNZ.total)  # 150.0

    priceAustralia = PriceAustralia(100)
    print(priceAustralia.total)  # 130.0

.. note:: 

    In above example, look for the PriceNZ class, where init function takes two arguments and one of which is used by descriptor using 'instance.net' command. Further, init function in class Total need one argument i.e. taxrate, which is passed by individual class which creating the object of the descriptor. 


Conclusion
----------

In this tutorial, we discussed data-descriptors and non-data-descriptors. Also, we saw the way values are stored in these two types of descriptors. Further, we saw that __getattribute__ method breaks the descriptor calls. 
