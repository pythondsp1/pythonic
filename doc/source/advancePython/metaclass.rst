Metaclasses
===========


Creating Metaclass
------------------

Metaclass can be crated by inheriting the class 'type' as shown below. Here __str__ method is overrided in class MyMetaclass. Note that, str commands prits "Hello" for class A, as it is inheriting the MyMetaclass, which modified the __str__ method. 

.. code-block:: python

    # metaclassEx.py

    class MyMetaclass(type):
        # use cls instead of self as we are creating classes
        # through metaclasses
        def __str__(cls):
            return "Hello"


    class A(metaclass=MyMetaclass):
        pass


    class B(object):
        pass


    print(A)  # Hello
    print(B)  # <class '__main__.B'>

.. note::

    Metaclass is created by inheriting the 'type' instead of 'object'. And we can change the behaviour of the class using metaclasses as shown in above example, where str works differently. 
