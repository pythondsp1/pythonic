Decorators
==========

Decorator is a function that creates  a wrapper around another function. This wrapper adds some additional functionality to existing code. In this tutorial, various types of decorators are discussed.  

Function inside the function and Decorator
------------------------------------------

Following is the example of function inside the function. 

.. code-block:: Python
    :linenos:

    # funcEx.py

    def addOne(myFunc):
        def addOneInside(x):
            print("adding One")
            return myFunc(x) + 1
        return addOneInside

    def subThree(x):
        return x - 3

    result = addOne(subThree)

    print(subThree(5))
    print(result(5))

    # outputs
    # 2
    # adding One
    # 3

Above code works as follows, 

* Function 'subThree' is defined at lines 9-10, which subtract the given number with 3. 
* Function 'addOne' (Line 3) has one argument i.e. myFunc, which indicates that 'addOne' takes the function as input. Since, subThree function has only one input argument, therefore one argument is set in the function 'addOneInside' (Line 4); which is used in return statement (Line 6). Also, "adding One" is printed before returning the value (Line 5).
* In line 12, return value of addOne (i.e. function 'addOneInside') is stored in 'result'. Hence, 'result' is a function which takes one input.   
* Lastly, values are printed at line 13 and 14. Note that "adding One" is printed by the result(5) and value is incremented by one i.e. 2 to 3. 


Another nice way of writing above code is shown below. Here (*args and **kwargs) are used, which takes all the arguments and keyword arguments of the function.

.. code-block:: Python
    :linenos:

    # funcEx.py

    def addOne(myFunc):
        def addOneInside(*args, **kwargs):
            print("adding One")
            return myFunc(*args, **kwargs) + 1
        return addOneInside

    def subThree(x):
        return x - 3

    result = addOne(subThree)

    print(subThree(5))
    print(result(5))

    # outputs
    # 2
    # adding One
    # 3


Now, in the below code, the return value of addOne is stored in the 'subThree' function itself (Line 12), 

.. code-block:: Python
    :linenos:

    # funcEx.py

    def addOne(myFunc):
        def addOneInside(*args, **kwargs):
            print("adding One")
            return myFunc(*args, **kwargs) + 1
        return addOneInside

    def subThree(x):
        return x - 3

    subThree = addOne(subThree)

    print(subThree(5))
    # outputs
    # adding One
    # 3

Lastly, in Python, the line 12 in above code, can be replaced by using decorator, as shown in Line 9 of below code, 

.. code-block:: Python
    :linenos:

    # funcEx.py

    def addOne(myFunc):
        def addOneInside(*args, **kwargs):
            print("adding One")
            return myFunc(*args, **kwargs) + 1
        return addOneInside

    @addOne
    def subThree(x):
        return x - 3

    print(subThree(5))
    # outputs
    # adding One
    # 3


In this section, we saw the basics of the decorator, which we will be used in this tutorial. 


Decorator without arguments
---------------------------

In following code, Decorator takes a function as the input and print the name of the function and return the function. 

.. code-block:: python

    # debugEx.py

    def printName(func):
        # func is the function to be wrapped
        def pn(*args, **kwargs):
            print(func.__name__)
            return func(*args, **kwargs)
        return pn

Next, put the printName function as decorator in the mathEx.py file as below, 

.. code-block:: python

    # mathEx.py

    from debugEx import printName

    @printName
    def add2Num(x, y):
        # add two numbers
        # print("add")
        return(x+y)

    print(add2Num(2, 4))
    help(add2Num)

Finally, execute the code and the name of each function will be printed before calculation as shown below, 

.. code-block:: shell

    $ python mathEx.py
    add
    6

    Help on function pn in module debugEx:

    pn(*args, **kwargs)
        # func is the function to be wrapped

.. important:: 

    Decorator brings all the debugging code at one places. Now we can add more debugging features to 'debugEx.py' file and all the changes will be applied immediately to all the functions. 


.. warning:: 

    Decorators remove the help features of the function along with name etc. Therefore, we need to fix it using functools as shown next. 

Rewrite the decorator using wraps function in functools as below, 

.. code-block:: python

    # debugEx.py

    from functools import wraps

    def printName(func):
        # func is the function to be wrapped
        
        # wrap is used to exchange metadata between functions
        @wraps(func)
        def pn(*args, **kwargs):
            print(func.__name__)
            return func(*args, **kwargs)
        return pn


If we execute the mathEx.py again, it will show the help features again. 

.. note::

    @wraps exchanges the metadata between the functions as shown in above example.


Decorators with arguments
-------------------------

Suppose, we want to pass some argument to the decorator as shown below, 

.. code-block:: python

    # mathEx.py

    from debugEx import printName

    @printName('**')
    def add2Num(x, y):
        '''add two numbers'''
        return(x+y)

    print(add2Num(2, 4))
    # help(add2Num)

.. note::

    To pass the argument to the decorator, all we need to write a outer function which takes the input arguments and then write the normal decorator inside that function as shown below, 

.. code-block:: python

    # debugEx.py

    from functools import wraps

    def printName(prefix=""):
        def addPrefix(func):
            msg = prefix + func.__name__
            # func is the function to be wrapped
            
            # wrap is used to exchange metadata between functions
            @wraps(func)
            def pn(*args, **kwargs):
                print(msg)
                return func(*args, **kwargs)
            return pn
        return addPrefix

Now, run the above code, 

.. code-block:: shell

    $ python mathEx.py 
    **add2Num
    6



.. rubric:: Error

* But, above code will generate error if we do not pass the argument to the decorator as shown below, 

.. code-block:: python

    # mathEx.py

    from debugEx import printName

    @printName
    def add2Num(x, y):
        '''add two numbers'''
        return(x+y)

    print(add2Num(2, 4))
    # help(add2Num)

Following error will be generate after running the code, 

.. code-block:: shell

    $ python mathEx.py 
    Traceback (most recent call last):
      File "mathEx.py", line 10, in <module>
        print(add2Num(2, 4))
    TypeError: addPrefix() takes 1 positional argument but 2 were given

* One solution is to write the two different codes e.g. 'printName' and 'printNameArg'; then use these decorators as required. **But this will make code repetitive** as shown below, 

 .. code-block:: python
 
     # debugEx.py

     from functools import wraps

     def printName(func):
         # func is the function to be wrapped
         
         # wrap is used to exchange metadata between functions
         @wraps(func)
         def pn(*args, **kwargs):
             print(func.__name__)
             return func(*args, **kwargs)
         return pn


     def printNameArg(prefix=""):
         def printName(func):
             # func is the function to be wrapped
             
             # wrap is used to exchange metadata between functions
             @wraps(func)
             def pn(*args, **kwargs):
                 print(prefix + func.__name__)
                 return func(*args, **kwargs)
             return pn
         return printName


Now, modify the math.py as below, 

.. code-block:: python

    # mathEx.py

    from debugEx import printName, printNameArg

    @printNameArg('**')
    def add2Num(x, y):
        '''add two numbers'''
        return(x+y)

    @printName
    def diff2Num(x, y):
        '''subtract two integers only'''
        return(x-y)

    print(add2Num(2, 4))
    print(diff2Num(2, 4))
    # help(add2Num)

Next execute the code, 

.. code-block:: shell

    $ python mathEx.py 
    **add2Num
    6
    diff2Num
    -2

DRY decorator with arguments
----------------------------

In previous code, we repeated the same code two time for creating the decorator with and without arguments. But, there is a better way to combine both the functionality in one decorator using partial function as shown below, 

.. code-block:: python 

    # debugEx.py

    from functools import wraps, partial

    def printName(func=None, *, prefix=""):
        if func is None:
            return partial(printName, prefix=prefix)    
        # wrap is used to exchange metadata between functions
        @wraps(func)
        def pn(*args, **kwargs):
            print(prefix + func.__name__)
            return func(*args, **kwargs)
        return pn

Now, modify the mathEx.py i.e. remove printNameArg decorator from the code, as below, 

.. code-block:: python 

    # mathEx.py

    from debugEx import printName

    @printName(prefix='**')
    def add2Num(x, y):
        '''add two numbers'''
        return(x+y)

    @printName
    def diff2Num(x, y):
        '''subtract two integers only'''
        return(x-y)

    print(add2Num(2, 4))
    print(diff2Num(2, 4))
    # help(add2Num)

Next, run the code and it will display following results, 

.. code-block:: shell

    $ python mathEx.py 
    **add2Num
    6
    diff2Num
    -2

Partial function is required because, when we pass argument to the decorator i.e. @printName(prifix='**'), then decorator will not find any function argument at first place, hence return func(*arg, **kwargs) will generate error as there is no 'func'.

To solve this problem, partial is used which returns the an new function, with modified parameters i.e. newFunc(func = printName, prefix = prefix).


Decorators inside the class
---------------------------

In previous sections, decorators are defined as functions. In this section, decorators will be defined as class methods. 

.. rubric:: class method and instance method decorator

In the following code, two types of decorators are defined inside the class i.e. using class method and instance method, 

.. code-block:: python
    :linenos:

    # clsDecorator.py

    from datetime import datetime 

    class DateDecorator(object):
        # instance method decorator
        def instantMethodDecorator(self, func):
            def printDate(*args, **kwargs):
                print("Instance method decorator at time : \n", datetime.today())
                return func(*args, **kwargs)
            return printDate

        # class method decorator
        @classmethod
        def classMethodDecorator(self, func):
            def printDate(*args, **kwargs):
                print("Class method decorator at time : \n", datetime.today())
                return func(*args, **kwargs)
            return printDate


    # decorator: instance method
    a = DateDecorator()
    @a.instantMethodDecorator
    def add(a, b):
        return a+b

    # decorator: class method
    @DateDecorator.classMethodDecorator
    def sub(a, b):
        return a-b

    sum = add(2, 3)
    # Instance method decorator at time : 
    #  2017-02-04 13:31:27.742283

    diff = sub(2, 3)
    # Class method decorator at time : 
    #  2017-02-04 13:31:27.742435


Note that, we need to instantiate the instance mehtod decorator before using it as shown at line 23; whereas class decorator can be used as ClassName.DecoratorName. 

Conclusion
----------

In this tutorial, we saw the relation between 'function inside the function' and decorator. Then, decorator with and without arguments are discussed. Lastly, class decorators are shown with examples. 