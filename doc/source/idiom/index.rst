Idioms
======

Each code shows the bad and good practices for writing the python code. 

Loop over the list
------------------

Avoid range command
^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    # multiply 2 to all elements of arr
    arr = [10, 20, 30]

    # bad practice
    for i in range(len(arr)):
        print(2*arr[i])  # 20, 40, 60

    # good practices
    for i in arr:
        print(2*i)  # 20, 40, 60

    # print in reverse order
    for i in reversed(arr):
        print(2*i)  # 60, 40, 20

Enumerate
^^^^^^^^^

In previous case, we do not have the access over indices. Use 'enumerate' to get access to indices as well, 

.. code-block:: python

    # multiply 2 to all elements of arr
    arr = [10, 20, 30]

    for i, a in enumerate(arr):
        print(i, ':', 2*a)
        # 0 : 20
        # 1 : 40
        # 2 : 60

Loop in sorted order
^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    # multiply 2 to all elements of arr,
    # but in sorted order
    arr = [10, 30, 50, 20]

    for i in sorted(arr):
        print(2*i)  # 20, 40, 60, 100

    # in reversed sorted order
    for i in sorted(arr, reverse=True):
        print(2*i)  # 100, 60, 40, 20

.. Loop over dictionaries
.. ----------------------

Loop over keys
^^^^^^^^^^^^^^

.. code-block:: python

    dc = { 'Toy':3, 'Play':4, 'Games':5}

    # print keys of dictionaries
    for d in dc:
        print(d)

Loop over keys and values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    dc = { 'Toy':3, 'Play':4, 'Games':5}

    # print keys, values of dictionaries
    for k, v in dc.items():
        print(k, v)
        # Toy 3
        # Play 4
        # Games 5

Create dictionaries from lists
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


.. code-block:: python

    k = ['Toy', 'Game', 'Tiger']
    v = ['Toys', 'Games', 'Tigers']

    #create dict
    dc = dict(zip(k, v))
    print(dc)
    # {'Game': 'Games', 'Tiger': 'Tigers', 'Toy': 'Toys'}

    d = dict(enumerate(v))
    print(d)
    # {0: 'Toys', 1: 'Games', 2: 'Tigers'}


Looping and modifying the list simultaneously
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We need to make a copy of the list for such operations as shown below, 

.. code-block:: python

    # loopUpdate.py

    animals = ['tiger', 'cat', 'dog']
    am = animals.copy()

    # below line will go in infinite loop
    # for a in animals: 
    for a in am:
        if len(a) > 3:
            animals.append(a)

    print(animals)

Or we can use 'animals[:]' in the for loop, instead of 'animal' as shown below, 

.. code-block:: python

    # loopUpdate.py

    animals = ['tiger', 'cat', 'dog']
 
    for a in animals[:]:
        if len(a) > 3:
            animals.append(a)

    print(animals)


Check items in the list
^^^^^^^^^^^^^^^^^^^^^^^

'in' keyword can be used with 'if' statement, to check the value in the list, 

.. code-block:: python

    # loopUpdate.py

    def testNumber(num):
        if num in [1, 3, 5]:
            print("Thanks")
        else:
            print("Number is not 1, 3 or 5")

    testNumber(3)
    testNumber(4)



Unpacking
---------

Any iterable i.e. list, tuple or set can be unpacked using assignment operator as below, 

.. code-block:: python

    >>> x = [1, 2, 3]
    >>> a, b, c = x
    >>> a
    1
    >>> b
    2

.. code-block:: python

    >>> student  = ["Tom", 90, 95, 98, 30]
    >>> Name, *Marks, Age = student
    >>> Marks
    [90, 95, 98]


.. code-block:: python

    >>> y = (1, "Two", 3, ("Five", "Six", "Seven"))
    >>> a, *b, (*c, d) = y
    >>> d
    'Seven'
    >>> c
    ['Five', 'Six']





Update variables
----------------

.. code-block:: python

    x = 3
    y = 2
    z = 5
    x, y, z = y, z, x

    print(x, y, z)  # 2, 5, 3